-- naming scheme: wire:(xp)(zp)(xm)(zm)(xpyp)(zpyp)(xmyp)(zmyp)(xpym)(zpym)(xmym)(zmym)_on/off
-- where x= x direction, z= z direction, y= y direction, p = +1, m = -1, e.g. xpym = {x=1, y=-1, z=0}
-- The (xp)/(zpyp)/.. statements shall be replaced by either 0 or 1
-- Where 0 means the wire has no visual connection to that direction and
-- 1 means that the wire visually connects to that other node.

local S = minetest.get_translator(minetest.get_current_modname())

-- #######################
-- ## Update wire looks ##
-- #######################

local wire_rules = {
	{x= 1, y= 0, z= 0, spread=true},	--xp
	{x= 0, y= 0, z= 1, spread=true},	--zp
	{x=-1, y= 0, z= 0, spread=true},	--xm
	{x= 0, y= 0, z=-1, spread=true},	--zm
	{x= 1, y= 1, z= 0},					--xpyp
	{x= 0, y= 1, z= 1},					--zpyp
	{x=-1, y= 1, z= 0},					--xmyp
	{x= 0, y= 1, z=-1},					--zmyp
	{x= 1, y=-1, z= 0},					--xpym
	{x= 0, y=-1, z= 1},					--zpym
	{x=-1, y=-1, z= 0},					--xmym
	{x= 0, y=-1, z=-1},					--zmym
	{x= 0, y=-1, z= 0, spread=true},	--ym (always)
}


-- self_pos = pos of any mesecon node, from_pos = pos of conductor to getconnect for
local function wire_getconnect(from_pos, self_pos, from_rule)
	local node = minetest.get_node(self_pos)
	if minetest.registered_nodes[node.name]
		and minetest.registered_nodes[node.name].mesecons or minetest.get_item_group(node.name, "opaque") == 1 then
		-- rules of node to possibly connect to
		local rules
		if (minetest.registered_nodes[node.name].mesecon_wire) then
			rules = wire_rules
		else
			rules = mesecon.get_any_rules(node)
		end

		for _, r in ipairs(mesecon.flattenrules(rules)) do
			if (vector.equals(vector.add(self_pos, r), from_pos)) then
				-- no upper-connect with opaqu block above me
				if from_rule.y==1 then
					local ufnode = mesecon.get_node_force(vector.add(from_pos, {x=0, y=1, z=0}))
					if minetest.get_item_group(ufnode.name, "opaque") == 1 then return false, 1 end
				end
				if from_rule.y==-1 then
					local nfnode = mesecon.get_node_force(vector.add(from_pos, {x=from_rule.x, y=0, z=from_rule.z}))
					if minetest.get_item_group(nfnode.name, "opaque") == 1 then return false, 2 end
				end

				-- receptor -> always
				if mesecon.is_receptor(node.name) then return true, 3 end
				-- conductor -> always
				if mesecon.is_conductor(node.name) then return true, 4 end

				if from_rule.x==1 or from_rule.x==-1 then
					local xpmnode = mesecon.get_node_force(vector.add(from_pos, {x=from_rule.x, y=0, z=0}))
					--check blocks z=+-1
					local zmnode = mesecon.get_node_force(vector.add(from_pos, {x=0, y=0, z=-1}))
					if (mesecon.is_receptor(zmnode.name) or mesecon.is_conductor(zmnode.name))  then return false, 5 end
					if minetest.get_item_group(zmnode.name, "opaque") == 1 then
						--check upper
						local zmypnode = mesecon.get_node_force(vector.add(from_pos, {x=0, y= 1, z=-1}))
						if mesecon.is_conductor(zmypnode.name) then return false, 6 end
					elseif zmnode.name=="air" then
						--check lower
						local zmymnode = mesecon.get_node_force(vector.add(from_pos, {x=0, y=-1, z=-1}))
						if mesecon.is_conductor(zmymnode.name) then return false, 7 end
					end

					local zpnode = mesecon.get_node_force(vector.add(from_pos, {x=0, y=0, z= 1}))
					if (mesecon.is_receptor(zpnode.name) or mesecon.is_conductor(zpnode.name)) then return false, 8 end
					if minetest.get_item_group(zpnode.name, "opaque") == 1 then
						--check upper
						local zpypnode = mesecon.get_node_force(vector.add(from_pos, {x=0, y=1, z= 1}))
						if mesecon.is_conductor(zpypnode.name) then return false, 9 end
					elseif zpnode.name=="air" then
						--check lower
						local zpymnode = mesecon.get_node_force(vector.add(from_pos, {x=0, y=-1, z= 1}))
						if mesecon.is_conductor(zpymnode.name) then return false, 10 end
					end
					if mesecon.is_effector(node.name) then
						--check block behind
						local xnode = mesecon.get_node_force(vector.add(from_pos, {x=from_rule.x*-1, y=0, z=0}))
						if xnode.name=="air" then
							--check lower
							xnode = mesecon.get_node_force(vector.add(from_pos, {x=from_rule.x*-1, y=-1, z=0}))
						elseif minetest.get_item_group(xnode.name, "opaque") == 1 then
							--check upper
							xnode = mesecon.get_node_force(vector.add(from_pos, {x=from_rule.x*-1, y= 1, z=0}))
						end
						if not(mesecon.is_receptor(xnode.name) or mesecon.is_conductor(xnode.name)) then return false end
					end
				end

				if from_rule.z==1 or from_rule.z==-1 then
					local zpmnode = mesecon.get_node_force(vector.add(from_pos, {x=0, y=0, z=from_rule.z}))
					--check blocks x=+-1
					local xmnode = mesecon.get_node_force(vector.add(from_pos, {x=-1, y=0, z=0}))
					if (mesecon.is_receptor(xmnode.name) or mesecon.is_conductor(xmnode.name)) then return false, 12 end
					if minetest.get_item_group(xmnode.name, "opaque") == 1 then
						--check upper
						local xmypnode = mesecon.get_node_force(vector.add(from_pos, {x=-1, y=1, z=0}))
						if mesecon.is_conductor(xmypnode.name) then return false, 13 end
					elseif xmnode.name=="air" then
						--check lower
						local xmymnode = mesecon.get_node_force(vector.add(from_pos, {x=-1, y=-1, z= 0}))
						if mesecon.is_conductor(xmymnode.name) then return false, 14 end
					end

					local xpnode = mesecon.get_node_force(vector.add(from_pos, {x= 1, y=0, z=0}))
					if (mesecon.is_receptor(xpnode.name) or mesecon.is_conductor(xpnode.name)) then return false, 15 end
					if minetest.get_item_group(xpnode.name, "opaque") == 1 then
						--check upper
						local xpypnode = mesecon.get_node_force(vector.add(from_pos, {x= 1, y=1, z=0}))
						if mesecon.is_conductor(xpypnode.name) then return false, 16 end
					elseif xpnode.name=="air" then
						--check lower
						local xpymnode = mesecon.get_node_force(vector.add(from_pos, {x=1, y=-1, z= 0}))
						if mesecon.is_conductor(xpymnode.name) then return false, 17 end
					end
					if mesecon.is_effector(node.name) then
						--check block behind
						local znode = mesecon.get_node_force(vector.add(from_pos, {x=0, y=0, z=from_rule.z*-1}))
						if znode.name=="air" then
							--check lower
							znode = mesecon.get_node_force(vector.add(from_pos, {x=from_rule.x*-1, y=-1, z=0}))
						elseif minetest.get_item_group(znode.name, "opaque") == 1 then
							--check upper
							znode = mesecon.get_node_force(vector.add(from_pos, {x=from_rule.x*-1, y= 1, z=0}))
						end
						if not(mesecon.is_receptor(znode.name) or mesecon.is_conductor(znode.name)) then return false end
					end
				end
				return true, 0
			end
		end
	end
	return false, -1
end

-- Update this node
local function wire_updateconnect(pos)
	local connections = {}
	--check all rules
	for _, r in ipairs(wire_rules) do
		local res, val = wire_getconnect(pos, vector.add(pos, r), r)
		if res then
			table.insert(connections, r)
		end
	end

	local nid = {}
	for _, vec in ipairs(connections) do
		-- flat component
		if vec.x ==  1 then nid[0] = "1" end
		if vec.z ==  1 then nid[1] = "1" end
		if vec.x == -1 then nid[2] = "1" end
		if vec.z == -1 then nid[3] = "1"  end

		-- slopy component
		if vec.y == 1 then
			if vec.x ==  1 then nid[4] = "1" end
			if vec.z ==  1 then nid[5] = "1" end
			if vec.x == -1 then nid[6] = "1" end
			if vec.z == -1 then nid[7] = "1" end
		elseif vec.y == -1 then
			if vec.x ==  1 then nid[ 8] = "1" end
			if vec.z ==  1 then nid[ 9] = "1" end
			if vec.x == -1 then nid[10] = "1" end
			if vec.z == -1 then nid[11] = "1" end

		end
	end

	local nodeid = 	  (nid[0] or "0")..(nid[1] or "0")..(nid[ 2] or "0")..(nid[ 3] or "0")
			        ..(nid[4] or "0")..(nid[5] or "0")..(nid[ 6] or "0")..(nid[ 7] or "0")
                    ..(nid[8] or "0")..(nid[9] or "0")..(nid[10] or "0")..(nid[11] or "0")

	local state_suffix = string.find(minetest.get_node(pos).name, "_off") and "_off" or "_on"
	local orules = mesecon.get_any_inputrules(mesecon.get_node_force(pos))
	mesecon.swap_node_force(pos, "mesecons:wire_"..nodeid..state_suffix)
	local nrules = mesecon.get_any_inputrules(mesecon.get_node_force(pos))
	local res = mesecon.rules_sub(orules, nrules)
	return res
end

local function update_on_place_dig(pos, node)
	-- Update placed node (get_node again as it may have been dug)
	local nn = minetest.get_node(pos)
	if (minetest.registered_nodes[nn.name])
	and (minetest.registered_nodes[nn.name].mesecon_wire) then
		wire_updateconnect(pos)
	end

	-- Update nodes around it
	local rules
	if minetest.registered_nodes[node.name]
	and minetest.registered_nodes[node.name].mesecon_wire then
		rules = wire_rules
	else
		rules = mesecon.get_any_rules(node)
	end
	if (not rules) then return end

	for _, r in ipairs(mesecon.flattenrules(rules)) do
		local np = vector.add(pos, r)
		local nnode = minetest.get_node(np).name
		if minetest.registered_nodes[nnode] and minetest.registered_nodes[nnode].mesecon_wire then
			local abandoned_rules = wire_updateconnect(np)
			if next(abandoned_rules) ~= nil then
				mesecon.conductor_off(np, abandoned_rules)
				mesecon.conductor_off(np, mesecon.conductor_get_rules(minetest.get_node(np)))
			end
		end
	end
end

mesecon.register_autoconnect_hook("wire", update_on_place_dig)

-- ############################
-- ## Wire node registration ##
-- ############################
-- Nodeboxes:
local box_center = {-1/16, -.5, -1/16, 1/16, -.5+1/64, 1/16}
local box_bump1 =  { -2/16, -8/16,  -2/16, 2/16, -.5+1/64, 2/16 }

local nbox_nid =
{
	[0] = {1/16, -.5, -1/16, 8/16, -.5+1/64, 1/16}, -- x positive
	[1] = {-1/16, -.5, 1/16, 1/16, -.5+1/64, 8/16}, -- z positive
	[2] = {-8/16, -.5, -1/16, -1/16, -.5+1/64, 1/16}, -- x negative
	[3] = {-1/16, -.5, -8/16, 1/16, -.5+1/64, -1/16}, -- z negative

	[4] = {.5-1/16, -.5+1/16, -1/16, .5, .4999+1/64, 1/16}, -- x positive up
	[5] = {-1/16, -.5+1/16, .5-1/16, 1/16, .4999+1/64, .5}, -- z positive up
	[6] = {-.5, -.5+1/16, -1/16, -.5+1/16, .4999+1/64, 1/16}, -- x negative up
	[7] = {-1/16, -.5+1/16, -.5, 1/16, .4999+1/64, -.5+1/16}  -- z negative up
}

local selectionbox =
{
	type = "fixed",
	fixed = {-.5, -.5, -.5, .5, -.5+1/16, .5}
}

-- go to the next nodeid (ex.: 01000011 --> 01000100)
local function nid_inc() end
function nid_inc(nid)
	local i = 0
	while nid[i-1] ~= 1 do
		nid[i] = (nid[i] ~= 1) and 1 or 0
		i = i + 1
	end

	-- BUT: Skip impossible nodeids:
	if ((nid[0] == 0 and nid[4] == 1) or (nid[1] == 0 and nid[5] == 1)
	or (nid[2] == 0 and nid[6] == 1) or (nid[3] == 0 and nid[7] == 1)) then
		return nid_inc(nid)
	end

	return i <= 8
end

local function register_wires()
	local nid = {}
	while true do
		-- Create group specifiction and nodeid string (see note above for details)
		local nodeid = 	  (nid[0] or "0")..(nid[1] or "0")..(nid[2] or "0")..(nid[3] or "0")
				        ..(nid[4] or "0")..(nid[5] or "0")..(nid[6] or "0")..(nid[7] or "0")

		-- Calculate nodebox
		local nodebox = {type = "fixed", fixed={box_center}}
		for i=0,7 do
			if nid[i] == 1 then
				table.insert(nodebox.fixed, nbox_nid[i])
			end
		end

		-- Add bump to nodebox if curved
		if (nid[0] == 1 and nid[1] == 1) or (nid[1] == 1 and nid[2] == 1)
		or (nid[2] == 1 and nid[3] == 1) or (nid[3] == 1 and nid[0] == 1) then
			table.insert(nodebox.fixed, box_bump1)
		end

		-- If nothing to connect to, still make a nodebox of a straight wire
		if nodeid == "00000000" then
			nodebox.fixed = {-8/16, -.5, -1/16, 8/16, -.5+1/16, 1/16}
		end

		local meseconspec_off = { conductor = {
			rules = wire_rules,
			state = mesecon.state.off,
			onstate = "mesecons:wire_"..nodeid.."_on"
		}}

		local meseconspec_on = { conductor = {
			rules = wire_rules,
			state = mesecon.state.on,
			offstate = "mesecons:wire_"..nodeid.."_off"
		}}

		local groups_on = {dig_immediate = 3, mesecon_conductor_craftable = 1,
			not_in_creative_inventory = 1, attached_node = 1, dig_by_water = 1,destroy_by_lava_flow=1, dig_by_piston = 1}
		local groups_off = {dig_immediate = 3, mesecon_conductor_craftable = 1,
			attached_node = 1, dig_by_water = 1,destroy_by_lava_flow=1, dig_by_piston = 1, craftitem = 1}
		if nodeid ~= "00000000" then
			groups_off["not_in_creative_inventory"] = 1
		end

		-- Wire textures
		local ratio_off = 128
		local ratio_on = 192
		local crossing_off = "(redstone_redstone_dust_dot.png^redstone_redstone_dust_line0.png^(redstone_redstone_dust_line1.png^[transformR90))^[colorize:#FF0000:"..ratio_off
		local crossing_on =  "(redstone_redstone_dust_dot.png^redstone_redstone_dust_line0.png^(redstone_redstone_dust_line1.png^[transformR90))^[colorize:#FF0000:"..ratio_on
		local straight0_off = "redstone_redstone_dust_line0.png^[colorize:#FF0000:"..ratio_off
		local straight0_on =  "redstone_redstone_dust_line0.png^[colorize:#FF0000:"..ratio_on
		local straight1_off = "redstone_redstone_dust_line0.png^[colorize:#FF0000:"..ratio_off
		local straight1_on =  "redstone_redstone_dust_line0.png^[colorize:#FF0000:"..ratio_on
		local dot_off = "redstone_redstone_dust_dot.png^[colorize:#FF0000:"..ratio_off
		local dot_on =  "redstone_redstone_dust_dot.png^[colorize:#FF0000:"..ratio_on

		local tiles_off, tiles_on

		local wirehelp, tt, longdesc, usagehelp, img, desc_off, desc_on
		if nodeid == "00000000" then
			-- Non-connected redstone wire
			nodebox.fixed = {-8/16, -.5, -8/16, 8/16, -.5+1/64, 8/16}
			-- “Dot” texture
			tiles_off = { dot_off, dot_off, "blank.png", "blank.png", "blank.png", "blank.png" }
			tiles_on = { dot_on, dot_on, "blank.png", "blank.png", "blank.png", "blank.png" }

			tt = S("Transmits redstone power, powers mechanisms")
			longdesc = S("Redstone is a versatile conductive mineral which transmits redstone power. It can be placed on the ground as a trail.").."\n"..
S("A redstone trail can be in two states: Powered or not powered. A powered redstone trail will power (and thus activate) adjacent redstone components.").."\n"..
S("Redstone power can be received from various redstone components, such as a block of redstone or a button. Redstone power is used to activate numerous mechanisms, such as redstone lamps or pistons.")
			usagehelp = S("Place redstone on the ground to build a redstone trail. The trails will connect to each other automatically and it can also go over hills.").."\n\n"..

S("Read the help entries on the other redstone components to learn how redstone components interact.")
			img = "redstone_redstone_dust.png"
			desc_off = S("Redstone")
			desc_on = S("Powered Redstone Spot (@1)", nodeid)
		else
			-- Connected redstone wire
			table.insert(nodebox, box_center)
			tiles_off = { crossing_off, crossing_off, straight0_off, straight1_off, straight0_off, straight1_off }
			tiles_on = { crossing_on, crossing_on, straight0_on, straight1_on, straight0_on, straight1_on }
			wirehelp = false
			desc_off = S("Redstone Trail (@1)", nodeid)
			desc_on = S("Powered Redstone Trail (@1)", nodeid)
		end

		mesecon.register_node(":mesecons:wire_"..nodeid, {
			drawtype = "nodebox",
			paramtype = "light",
			use_texture_alpha = minetest.features.use_texture_alpha_string_modes and "clip" or true,
			sunlight_propagates = true,
			selection_box = selectionbox,
			node_box = nodebox,
			walkable = false,
			drop = "mesecons:wire_00000000_off",
			sounds = mcl_sounds.node_sound_defaults(),
			is_ground_content = false,
			mesecon_wire = true
		},{
			description = desc_off,
			inventory_image = img,
			wield_image = img,
			_tt_help = tt,
			_doc_items_create_entry = wirehelp,
			_doc_items_longdesc = longdesc,
			_doc_items_usagehelp = usagehelp,
			tiles = tiles_off,
			mesecons = meseconspec_off,
			groups = groups_off,
		},{
			description = desc_on,
			_doc_items_create_entry = false,
			tiles = tiles_on,
			mesecons = meseconspec_on,
			groups = groups_on
		})

		-- Add Help entry aliases for e.g. making it identifiable by the lookup tool [doc_identifier]
		if minetest.get_modpath("doc") then
			if nodeid ~= "00000000" then
				doc.add_entry_alias("nodes", "mesecons:wire_00000000_off", "nodes", "mesecons:wire_"..nodeid.."_off")
			end
			doc.add_entry_alias("nodes", "mesecons:wire_00000000_off", "nodes", "mesecons:wire_"..nodeid.."_on")
		end

		if (nid_inc(nid) == false) then return end
	end
end
-- register the old nodes -> alias?
register_wires()


----------------------------------------------------

-- go to the next nodeid (ex.: 01000011 --> 01000100)
local function nid_inc2() end
function nid_inc2(nid)
	local i = 0
	while nid[i-1] ~= 1 do
		nid[i] = (nid[i] ~= 1) and 1 or 0
		i = i + 1
	end

	-- BUT: Skip impossible nodeids:
	if (
		(nid[0] == 0 and nid[4] == 1) or (nid[0] == 0 and nid[ 8] == 1) or (nid[4] == 1 and nid[ 8] == 1) or
		(nid[1] == 0 and nid[5] == 1) or (nid[1] == 0 and nid[ 9] == 1) or (nid[5] == 1 and nid[ 9] == 1) or
		(nid[2] == 0 and nid[6] == 1) or (nid[2] == 0 and nid[10] == 1) or (nid[6] == 1 and nid[10] == 1) or
		(nid[3] == 0 and nid[7] == 1) or (nid[3] == 0 and nid[11] == 1) or (nid[7] == 1 and nid[11] == 1)
		) then
		return nid_inc2(nid)
	end

	return i <= 12
end


local function register_wires2()
	local nid = {}
	while true do
		-- Create group specifiction and nodeid string (see note above for details)
		local nodeid = 	  (nid[0] or "0")..(nid[1] or "0")..(nid[ 2] or "0")..(nid[ 3] or "0")
				        ..(nid[4] or "0")..(nid[5] or "0")..(nid[ 6] or "0")..(nid[ 7] or "0")
						..(nid[8] or "0")..(nid[9] or "0")..(nid[10] or "0")..(nid[11] or "0")

		-- Calculate nodebox, ignore 8 to 11
		local nodebox = {type = "fixed", fixed={box_center}}
		for i=0,7 do
			if nid[i] == 1 then
				table.insert(nodebox.fixed, nbox_nid[i])
			end
		end

		-- Add bump to nodebox if curved
		if (nid[0] == 1 and nid[1] == 1) or (nid[1] == 1 and nid[2] == 1)
		or (nid[2] == 1 and nid[3] == 1) or (nid[3] == 1 and nid[0] == 1) then
			table.insert(nodebox.fixed, box_bump1)
		end

		-- If nothing to connect to, still make a nodebox of a straight wire
		--if nodeid == "00000000" then
		if nodeid:sub(1,8) == "00000000" then
			nodebox.fixed = {-8/16, -.5, -1/16, 8/16, -.5+1/16, 1/16}
		end
		--rules berechnen, nur nötige verwenden
		local nrules = {}
		for i=0,11 do
			if nid[i]==1 then
				table.insert(nrules,wire_rules[i+1])
			end
		end
		table.insert(nrules,wire_rules[12+1])	--ym (always)

--[[
00100000


-- naming scheme: wire:(xp)(zp)(xm)(zm)(xpyp)(zpyp)(xmyp)(zmyp)_on/off


komplett flat
11110000

 {x= 1,  y= 0, z= 0, spread=true}, --xp	0	x
 {x= 0,  y= 0, z= 1, spread=true}, --zp	1	-
 {x=-1,  y= 0, z= 0, spread=true}, --xm	2	x
 {x= 0,  y= 0, z=-1, spread=true}, --zm	3


{x= 0,  y=-1, z= 0, spread=true},	--unten (immer)



 {x= 1, y= 1, z= 0},	--xpyp	4
 {x= 0, y= 1, z= 1},	--zpyp	5
 {x=-1, y= 1, z= 0},	--xmyp	6
 {x= 0, y= 1, z=-1},	--zmyp	7

 {x= 1, y=-1, z= 0},	--xpym	8
 {x= 0, y=-1, z= 1},	--zpym	9
 {x=-1, y=-1, z= 0},	--xmym	10
 {x= 0, y=-1, z=-1},	--zmym	11

0,4, 8
1,5, 9
2,6,10
3,7,11
--]]

--[[
 {x= 1, y= 0, z= 0, spread=true},	--xp
 {x= 0, y= 0, z= 1, spread=true},	--zp
 {x=-1, y= 0, z= 0, spread=true},	--xm
 {x= 0, y= 0, z=-1, spread=true},  --zm
 {x= 1, y= 1, z= 0},				--xpyp
 {x= 0, y= 1, z= 1},				--zpyp
 {x=-1, y= 1, z= 0},				--xmyp
 {x= 0, y= 1, z=-1},				--zmyp
 {x= 1, y=-1, z= 0},				--xpym
 {x= 0, y=-1, z= 1},				--zpym
 {x=-1, y=-1, z= 0},				--xmym
 {x= 0, y=-1, z=-1}}				--zmym
 {x= 0, y=-1, z= 0, spread=true},	--ym (always)

--]]

		local meseconspec_off = { conductor = {
			--rules = wire_rules,
			rules = nrules,
			state = mesecon.state.off,
			onstate = "mesecons:wire_"..nodeid.."_on"
		}}

		local meseconspec_on = { conductor = {
			--rules = wire_rules,
			rules = nrules,
			state = mesecon.state.on,
			offstate = "mesecons:wire_"..nodeid.."_off"
		}}

		local groups_on = {dig_immediate = 3, mesecon_conductor_craftable = 1,
			not_in_creative_inventory = 1, attached_node = 1, dig_by_water = 1,destroy_by_lava_flow=1, dig_by_piston = 1}
		local groups_off = {dig_immediate = 3, mesecon_conductor_craftable = 1,
			attached_node = 1, dig_by_water = 1,destroy_by_lava_flow=1, dig_by_piston = 1, craftitem = 1}
		--if nodeid ~= "00000000" then
		if nodeid:sub(1,8) ~= "00000000" then
			groups_off["not_in_creative_inventory"] = 1
		end

		-- Wire textures
		local ratio_off = 128
		local ratio_on = 192
		local crossing_off = "(redstone_redstone_dust_dot.png^redstone_redstone_dust_line0.png^(redstone_redstone_dust_line1.png^[transformR90))^[colorize:#FF0000:"..ratio_off
		local crossing_on = "(redstone_redstone_dust_dot.png^redstone_redstone_dust_line0.png^(redstone_redstone_dust_line1.png^[transformR90))^[colorize:#FF0000:"..ratio_on
		local straight0_off = "redstone_redstone_dust_line0.png^[colorize:#FF0000:"..ratio_off
		local straight0_on = "redstone_redstone_dust_line0.png^[colorize:#FF0000:"..ratio_on
		local straight1_off = "redstone_redstone_dust_line0.png^[colorize:#FF0000:"..ratio_off
		local straight1_on = "redstone_redstone_dust_line0.png^[colorize:#FF0000:"..ratio_on
		local dot_off = "redstone_redstone_dust_dot.png^[colorize:#FF0000:"..ratio_off
		local dot_on = "redstone_redstone_dust_dot.png^[colorize:#FF0000:"..ratio_on

		local tiles_off, tiles_on

		local wirehelp, tt, longdesc, usagehelp, img, desc_off, desc_on
		if nodeid:sub(1,8) == "00000000" then
			-- Non-connected redstone wire
			nodebox.fixed = {-8/16, -.5, -8/16, 8/16, -.5+1/64, 8/16}
			-- “Dot” texture
			tiles_off = { dot_off, dot_off, "blank.png", "blank.png", "blank.png", "blank.png" }
			tiles_on = { dot_on, dot_on, "blank.png", "blank.png", "blank.png", "blank.png" }

			tt = S("Transmits redstone power, powers mechanisms")
			longdesc =  S("Redstone is a versatile conductive mineral which transmits redstone power. It can be placed on the ground as a trail.").."\n"..
					    S("A redstone trail can be in two states: Powered or not powered. A powered redstone trail will power (and thus activate) adjacent redstone components.").."\n"..
					    S("Redstone power can be received from various redstone components, such as a block of redstone or a button. Redstone power is used to activate numerous mechanisms, such as redstone lamps or pistons.")
			usagehelp = S("Place redstone on the ground to build a redstone trail. The trails will connect to each other automatically and it can also go over hills.").."\n\n"..

S("Read the help entries on the other redstone components to learn how redstone components interact.")
			img = "redstone_redstone_dust.png"
			desc_off = S("Redstone")
			desc_on = S("Powered Redstone Spot (@1)", nodeid)
		else
			-- Connected redstone wire
			table.insert(nodebox, box_center)
			tiles_off = { crossing_off, crossing_off, straight0_off, straight1_off, straight0_off, straight1_off }
			tiles_on = { crossing_on, crossing_on, straight0_on, straight1_on, straight0_on, straight1_on }
			wirehelp = false
			desc_off = S("Redstone Trail (@1)", nodeid)
			desc_on = S("Powered Redstone Trail (@1)", nodeid)
		end

		mesecon.register_node(":mesecons:wire_"..nodeid, {
			drawtype = "nodebox",
			paramtype = "light",
			use_texture_alpha = minetest.features.use_texture_alpha_string_modes and "clip" or true,
			sunlight_propagates = true,
			selection_box = selectionbox,
			node_box = nodebox,
			walkable = false,
			drop = "mesecons:wire_00000000_off",
			sounds = mcl_sounds.node_sound_defaults(),
			is_ground_content = false,
			mesecon_wire = true
		},{
			description = desc_off,
			inventory_image = img,
			wield_image = img,
			_tt_help = tt,
			_doc_items_create_entry = wirehelp,
			_doc_items_longdesc = longdesc,
			_doc_items_usagehelp = usagehelp,
			tiles = tiles_off,
			mesecons = meseconspec_off,
			groups = groups_off,
		},{
			description = desc_on,
			_doc_items_create_entry = false,
			tiles = tiles_on,
			mesecons = meseconspec_on,
			groups = groups_on
		})

		-- Add Help entry aliases for e.g. making it identifiable by the lookup tool [doc_identifier]
		if minetest.get_modpath("doc") then
			if nodeid:sub(1,8) ~= "00000000" then
				doc.add_entry_alias("nodes", "mesecons:wire_000000000000_off", "nodes", "mesecons:wire_"..nodeid.."_off")
			end
			doc.add_entry_alias("nodes", "mesecons:wire_000000000000_off", "nodes", "mesecons:wire_"..nodeid.."_on")
		end

		if (nid_inc2(nid) == false) then return end
	end
end

register_wires2()

----------------------------------------------------


minetest.register_alias("mesecons:redstone", "mesecons:wire_00000000_off")

minetest.register_craft({
	type = "cooking",
	output = "mesecons:redstone",
	recipe = "mcl_core:stone_with_redstone",
	cooktime = 10,
})

