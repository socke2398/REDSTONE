local S = minetest.get_translator(minetest.get_current_modname())

-- Functions that get the input/output rules of the comparator

local function comparator_get_output_rules(node)
	local rules = {{x = -1, y = 0, z = 0, spread=true}}
	for i = 0, node.param2 do
		rules = mesecon.rotate_rules_left(rules)
	end
	return rules
end


local function comparator_get_input_rules(node)
	local rules = {
		-- we rely on this order in update_self below
		{x = 1, y = 0, z =  0},  -- back
		{x = 0, y = 0, z = -1},  -- side
		{x = 0, y = 0, z =  1},  -- side
	}
	for i = 0, node.param2 do
		rules = mesecon.rotate_rules_left(rules)
	end
	return rules
end


-- Functions that are called after the delay time

local function comparator_turnon(params)
	local rules = comparator_get_output_rules(params.node)
	if params.lower_voltage then
		for _, rule in ipairs(mesecon.flattenrules(rules)) do
			local np = vector.add(params.pos, rule)
			local rulenames = mesecon.rules_link_rule_all(params.pos, rule)
			for _, rulename in ipairs(rulenames) do
				mesecon.conductor_off(np)
			end
		end
	end
	mesecon.receptor_on(params.pos, rules, params.voltage)
end

local function comparator_turnoff(params)
	local rules = comparator_get_output_rules(params.node)
	mesecon.receptor_off(params.pos, rules)
end


-- Functions that set the correct node type an schedule a turnon/off

local function comparator_activate(pos, node, voltage, lower_voltage, old_voltage)
	local def = minetest.registered_nodes[node.name]
	minetest.swap_node(pos, { name = def.comparator_onstate, param2 = node.param2 })
	minetest.after(0.1, comparator_turnon , {pos = pos, node = node, voltage = voltage, lower_voltage=lower_voltage, old_voltage=old_voltage})
end


local function comparator_deactivate(pos, node)
	local def = minetest.registered_nodes[node.name]
	minetest.swap_node(pos, { name = def.comparator_offstate, param2 = node.param2 })
	minetest.after(0.1, comparator_turnoff, {pos = pos, node = node})
end


-- weather pos has an inventory that contains at least one item
local function container_inventory_nonempty(pos, rule)
	--    https://minecraft.fandom.com/wiki/Redstone_Comparator#Measure_block_state
	--    signal strength = floor(1 + ((sum of all slots' fullnesses) / (number of slots in container)) × 14)
    --    fullness of a slot = number of items in slot / max stack size for this type of item

	local invnode = minetest.get_node(pos)
	local invnodedef = minetest.registered_nodes[invnode.name]
	-- Ignore stale nodes
	if not invnodedef then return 0, false end

	-- Only accept containers. When a container is dug, it's inventory
	-- seems to stay. and we don't want to accept the inventory of an air
	-- block
	if not invnodedef.groups.container then
		if minetest.get_item_group(invnode.name, "opaque") == 1 and rule then
			--found opaque block -> check next block
			local backback_pos = vector.add(pos, rule)
			local bb_voltage, _ = container_inventory_nonempty(backback_pos, nil)
			return bb_voltage, true
		else
			return 0, false
		end
	end

	local inv = minetest.get_inventory({type="node", pos=pos})
	if (not inv) or inv:is_empty("main") then return 0, false end

	if invnode.name=="mcl_jukebox:jukebox" then
		local record = inv:get_stack("main", 1)
		if  mcl_jukebox.registered_records[record:get_name()] then
			local ident = mcl_jukebox.registered_records[record:get_name()][3]
			local t = {
				id_13 		=  1,
				id_cat 		=  2,
				id_blocks	=  3,
				id_chirp	=  4,
				id_far		=  5,
				id_mall		=  6,
				id_mellohi	=  7,
				id_stal		=  8,
				id_strad	=  9,
				id_ward		= 10,
				id_11		= 11,
				id_wait		= 12,
				id_Pigstep	= 13,
			}
			local vol = t["id_"..ident]
			if vol then return vol, false end
		end
	elseif invnode.name=="mcl_itemframes:item_frame" then
		local meta = minetest.get_meta(pos)
		local rotation = meta:get_int("rotation") or 0
		return rotation+1, false
	else
		local item_count = 0
		local inv_space  = 0
		for listname, _ in pairs(inv:get_lists()) do
			local stack = inv:get_stack(listname, 1)
			local st_size = inv:get_size(listname)
			inv_space = inv_space + (64 * st_size)
			for i = 1,st_size do
				local stack = inv:get_stack(listname, i)
				item_count = item_count + (stack:get_count() * 64 / stack:get_stack_max())
			end
		end
		if item_count>0 then
			local voltage = math.floor(1 + (item_count/inv_space*14))
			return voltage, false
		end
	end
	return 0, false
end

-- weather pos has an constant signal output for the comparator
local function static_signal_output(pos)
	local node = minetest.get_node(pos)
	local g = minetest.get_item_group(node.name, "comparator_signal")
	return g
end

-- whether the comparator should be on according to its inputs
local function comparator_desired_on(pos, node)
	local my_input_rules = comparator_get_input_rules(node);
	local back_rule = my_input_rules[1]
	local voltage = 0
	if back_rule then
		local back_pos = vector.add(pos, back_rule)
		local _, _, vo_back = mesecon.is_powered(pos, back_rule)
		local vo_coin, spread = container_inventory_nonempty(back_pos, back_rule)
		local vo_sso = static_signal_output(back_pos)
		if vo_coin>=1 and spread and vo_back<15 then
			--container through opaque block and block is powerd less than 15 -> ignore powered block
			vo_back=0
		end
		voltage = math.max(vo_back, vo_coin, vo_sso)
	end

	-- if back input is off, we don't need to check side inputs
	if voltage<1 then return false, 0 end

	local side_voltage = 0
	for ri = 2,3 do
		if my_input_rules[ri] then
			for _, l in pairs(mesecon.rules_link_rule_all_inverted(pos, my_input_rules[ri])) do
				local _, s_voltage = mesecon.is_power_on(vector.add(pos, my_input_rules[ri]))
				side_voltage = math.max(side_voltage, s_voltage)
			end

		end
	end
	local mode = minetest.registered_nodes[node.name].comparator_mode
	if mode == "comp" then
		-- Comparators in comparison mode
		if side_voltage > voltage then
			return false, 0
		else
			return true, voltage
		end
	end

	-- comparator in subtraction mode
	voltage = math.max(voltage-side_voltage, 0)
	if voltage>0 then
		return true, voltage
	else
		return false, 0
	end
end

--[[
Compare signal strength

Comparators in comparison mode.

A redstone comparator in comparison mode (front torch down and unpowered) compares its rear input to its two side inputs.
If either side input is greater than the rear input, the comparator output turns off. If neither side input is greater than
the rear input, the comparator outputs the same signal strength as its rear input.

The formula for calculating the output signal strength is as follows:

output = rear × [left ≤ rear AND right ≤ rear]


Subtract signal strength

A redstone comparator in subtraction mode (front torch up and powered) subtracts the signal strength of the higher side input from the signal strength of the rear input.

output = max(rear − max(left, right), 0)

For example: if the signal strength is 6 at the left input, 7 at the right input and 4 at the rear, the output signal has a strength of max(4 − max(6, 7), 0) = max(4−7, 0) = max(−3, 0) = 0.

If the signal strength is 9 at the rear, 2 at the right input and 5 at the left input, the output signal has a strength of max(9 − max(2, 5), 0) = max(9−5, 0) = 4. 




--]]
-- update comparator state, if needed
local function update_self(pos, node)
	node = node or minetest.get_node(pos)
	local old_state = mesecon.is_receptor_on(node.name)
	local meta = minetest.get_meta(pos)
	local old_voltage = old_state and (meta:get_string("mesecon_voltage")~="" and meta:get_int("mesecon_voltage") or 15) or 0
	local new_state, new_voltage = comparator_desired_on(pos, node)
	if (new_state ~= old_state) or (old_voltage~=new_voltage) then
		if old_voltage~=new_voltage then meta:set_int("mesecon_voltage", new_voltage) end
		if new_state then
			comparator_activate(pos, node, new_voltage, (new_voltage<old_voltage), old_voltage)
		else
			comparator_deactivate(pos, node)
		end
	end
end


-- compute tile depending on state and mode
local function get_tiles(state, mode)
	local top = "mcl_comparators_"..state..".png^"..
		"mcl_comparators_"..mode..".png"
	local sides = "mcl_comparators_sides_"..state..".png^"..
		"mcl_comparators_sides_"..mode..".png"
	local ends = "mcl_comparators_ends_"..state..".png^"..
		"mcl_comparators_ends_"..mode..".png"
	return {
		top, "mcl_stairs_stone_slab_top.png",
		sides, sides.."^[transformFX",
		ends, ends,
	}
end

-- Given one mode, get the other mode
local function flipmode(mode)
	if mode == "comp" then    return "sub"
	elseif mode == "sub" then return "comp"
	end
end

local function make_rightclick_handler(state, mode)
	local newnodename =
		"mcl_comparators:comparator_"..state.."_"..flipmode(mode)
	return function (pos, node, clicker)
		local protname = clicker:get_player_name()
		if minetest.is_protected(pos, protname) then
			minetest.record_protection_violation(pos, protname)
			return
		end
		minetest.swap_node(pos, {name = newnodename, param2 = node.param2 })
	end
end


-- Register the 2 (states) x 2 (modes) comparators

local icon = "mcl_comparators_item.png"

local node_boxes = {
	comp = {
		{ -8/16, -8/16, -8/16,
		   8/16, -6/16,  8/16 },	-- the main slab
		{ -1/16, -6/16,  6/16,
		   1/16, -4/16,  4/16 },	-- front torch
		{ -4/16, -6/16, -5/16,
		  -2/16, -1/16, -3/16 },	-- left back torch
		{  2/16, -6/16, -5/16,
		   4/16, -1/16, -3/16 },	-- right back torch
	},
	sub = {
		{ -8/16, -8/16, -8/16,
		   8/16, -6/16,  8/16 },	-- the main slab
		{ -1/16, -6/16,  6/16,
		   1/16, -3/16,  4/16 },	-- front torch (active)
		{ -4/16, -6/16, -5/16,
		  -2/16, -1/16, -3/16 },	-- left back torch
		{  2/16, -6/16, -5/16,
		   4/16, -1/16, -3/16 },	-- right back torch
	},
}

local collision_box = {
	type = "fixed",
	fixed = { -8/16, -8/16, -8/16, 8/16, -6/16, 8/16 },
}

local state_strs = {
	[ mesecon.state.on  ] = "on",
	[ mesecon.state.off ] = "off",
}

local groups = {
	dig_immediate = 3,
	dig_by_water  = 1,
	destroy_by_lava_flow = 1,
	dig_by_piston = 1,
	attached_node = 1,
}

local on_rotate
if minetest.get_modpath("screwdriver") then
	on_rotate = screwdriver.disallow
end

for _, mode in pairs{"comp", "sub"} do
	for _, state in pairs{mesecon.state.on, mesecon.state.off} do
		local state_str = state_strs[state]
		local nodename =
			"mcl_comparators:comparator_"..state_str.."_"..mode

		-- Help
		local longdesc, usagehelp, use_help
		if state_str == "off" and mode == "comp" then
			longdesc = S("Redstone comparators are multi-purpose redstone components.").."\n"..
			S("They can transmit a redstone signal, detect whether a block contains any items and compare multiple signals.")

			usagehelp = S("A redstone comparator has 1 main input, 2 side inputs and 1 output. The output is in arrow direction, the main input is in the opposite direction. The other 2 sides are the side inputs.").."\n"..
				S("The main input can powered in 2 ways: First, it can be powered directly by redstone power like any other component. Second, it is powered if, and only if a container (like a chest) is placed in front of it and the container contains at least one item.").."\n"..
				S("The side inputs are only powered by normal redstone power. The redstone comparator can operate in two modes: Transmission mode and subtraction mode. It starts in transmission mode and the mode can be changed by using the block.").."\n\n"..
				S("Transmission mode:\nThe front torch is unlit and lowered. The output is powered if, and only if the main input is powered. The two side inputs are ignored.").."\n"..
				S("Subtraction mode:\nThe front torch is lit. The output is powered if, and only if the main input is powered and none of the side inputs is powered.")
		else
			use_help = false
		end

		local nodedef = {
			description = S("Redstone Comparator"),
			inventory_image = icon,
			wield_image = icon,
			_doc_items_create_entry = use_help,
			_doc_items_longdesc = longdesc,
			_doc_items_usagehelp = usagehelp,
			drawtype = "nodebox",
			tiles = get_tiles(state_str, mode),
			use_texture_alpha = minetest.features.use_texture_alpha_string_modes and "opaque" or false,
			--wield_image = "mcl_comparators_off.png",
			walkable = true,
			selection_box = collision_box,
			collision_box = collision_box,
			node_box = {
				type = "fixed",
				fixed = node_boxes[mode],
			},
			groups = groups,
			paramtype = "light",
			paramtype2 = "facedir",
			sunlight_propagates = false,
			is_ground_content = false,
			drop = "mcl_comparators:comparator_off_comp",
			on_construct = update_self,
			on_rightclick =
				make_rightclick_handler(state_str, mode),
			comparator_mode = mode,
			comparator_onstate = "mcl_comparators:comparator_on_"..mode,
			comparator_offstate = "mcl_comparators:comparator_off_"..mode,
			sounds = mcl_sounds.node_sound_stone_defaults(),
			mesecons = {
				receptor = {
					state = state,
					rules = comparator_get_output_rules,
					opaquespread = true,
				},
				effector = {
					rules = comparator_get_input_rules,
					action_change = update_self,
				}
			},
			on_rotate = on_rotate,
		}

		if state == mesecon.state.on then
			nodedef.on_destruct = function(pos, oldnode)
				local node = minetest.get_node(pos)
				mesecon.on_dignode(pos, node)
			end
		end
		if mode == "comp" and state == mesecon.state.off then
			-- This is the prototype
			nodedef._doc_items_create_entry = true
		else
			nodedef.groups = table.copy(nodedef.groups)
			nodedef.groups.not_in_creative_inventory = 1
			--local extra_desc = {}
			if mode == "sub" or state == mesecon.state.on then
				nodedef.inventory_image = nil
			end
			local desc = nodedef.description
			if mode ~= "sub" and state == mesecon.state.on then
				desc = S("Redstone Comparator (Powered)")
			elseif mode == "sub" and state ~= mesecon.state.on then
				desc = S("Redstone Comparator (Subtract)")
			elseif mode == "sub" and state == mesecon.state.on then
				desc = S("Redstone Comparator (Subtract, Powered)")
			end
			nodedef.description = desc
		end

		minetest.register_node(nodename, nodedef)
		mcl_wip.register_wip_item(nodename)
	end
end

-- Register recipies
local rstorch = "mesecons_torch:mesecon_torch_on"
local quartz  = "mcl_nether:quartz"
local stone   = "mcl_core:stone"

minetest.register_craft({
	output = "mcl_comparators:comparator_off_comp",
	recipe = {
		{ "",      rstorch, ""      },
		{ rstorch, quartz,  rstorch },
		{ stone,   stone,   stone   },
	}
})

-- Register active block handlers
minetest.register_abm({
	label = "Comparator signal input check (comparator is off)",
	nodenames = {
		"mcl_comparators:comparator_off_comp",
		"mcl_comparators:comparator_off_sub",
	},
	neighbors = {"group:container", "group:comparator_signal", "group:opaque", "mcl_jukebox:jukebox"},
	interval = 1,
	chance = 1,
	action = update_self,
})

minetest.register_abm({
	label = "Comparator signal input check (comparator is on)",
	nodenames = {
		"mcl_comparators:comparator_on_comp",
		"mcl_comparators:comparator_on_sub",
	},
	-- needs to run regardless of neighbors to make sure we detect when a
	-- container is dug
	interval = 1,
	chance = 1,
	action = update_self,
})


-- Add entry aliases for the Help
if minetest.get_modpath("doc") then
	doc.add_entry_alias("nodes", "mcl_comparators:comparator_off_comp",
				"nodes", "mcl_comparators:comparator_off_sub")
	doc.add_entry_alias("nodes", "mcl_comparators:comparator_off_comp",
				"nodes", "mcl_comparators:comparator_on_comp")
	doc.add_entry_alias("nodes", "mcl_comparators:comparator_off_comp",
				"nodes", "mcl_comparators:comparator_on_sub")
end
