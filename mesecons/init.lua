-- |\    /| ____ ____  ____ _____   ____         _____
-- | \  / | |    |     |    |      |    | |\   | |
-- |  \/  | |___ ____  |___ |      |    | | \  | |____
-- |      | |        | |    |      |    | |  \ |     |
-- |      | |___ ____| |___ |____  |____| |   \| ____|
-- by Jeija, Uberi (Temperest), sfan5, VanessaE, Hawk777 and contributors
--
-- extended by socke2398
--
-- This mod adds mesecons[=minecraft redstone] and different receptors/effectors to minetest.
-- See the documentation on the forum for additional information, especially about crafting
--
--
-- For basic development resources, see http://mesecons.net/developers.html
--
--
--
--Quick draft for the mesecons array in the node's definition
--mesecons =
--{
--	receptor =
--	{
--		state = mesecon.state.on/off
--		rules = rules/get_rules
--	},
--	effector =
--	{
--		action_on = function
--		action_off = function
--		action_change = function
--		rules = rules/get_rules
--	},
--	conductor =
--	{
--		state = mesecon.state.on/off
--		offstate = opposite state (for state = on only)
--		onstate = opposite state (for state = off only)
--		rules = rules/get_rules
--	}
--}

-- PUBLIC VARIABLES
mesecon={} -- contains all functions and all global variables
mesecon.queue={} -- contains the ActionQueue
mesecon.queue.funcs={} -- contains all ActionQueue functions

-- Settings
dofile(minetest.get_modpath("mesecons").."/settings.lua")

-- Utilities like comparing positions,
-- adding positions and rules,
-- mostly things that make the source look cleaner
dofile(minetest.get_modpath("mesecons").."/util.lua");

-- Presets (eg default rules)
dofile(minetest.get_modpath("mesecons").."/presets.lua");

-- The ActionQueue
-- Saves all the actions that have to be execute in the future
dofile(minetest.get_modpath("mesecons").."/actionqueue.lua");

-- Internal stuff
-- This is the most important file
-- it handles signal transmission and basically everything else
-- It is also responsible for managing the nodedef things,
-- like calling action_on/off/change
dofile(minetest.get_modpath("mesecons").."/internal.lua");

-- API
-- these are the only functions you need to remember

mesecon.queue:add_function("receptor_on", function (pos, rules, voltage, opaquespread)
	mesecon.vm_begin()

	rules = rules or mesecon.rules.default

	local node = mesecon.get_node_force(pos)
	local os=opaquespread or mesecon.is_receptor_opaquespread(node.name)
	-- Call turnon on all linking positions
	for _, rule in pairs(mesecon.flattenrules(rules)) do
		local np = vector.add(pos, rule)
		local rulenames = mesecon.rules_link_rule_all(pos, rule)
		for _, rulename in pairs(rulenames) do
			if os then rulename.opaquespread=os end
			mesecon.turnon(np, rulename, voltage)
		end
	end

	mesecon.vm_commit()
end)

function mesecon.receptor_on(pos, rules, voltage, opaquespread)
	mesecon.queue:add_action(pos, "receptor_on", {rules, voltage, opaquespread}, nil, rules)
end

mesecon.queue:add_function("receptor_off", function (pos, rules, opaquespread)
	rules = rules or mesecon.rules.default

	local node = mesecon.get_node_force(pos)
	local os=opaquespread or mesecon.is_receptor_opaquespread(node.name)
	-- Call turnoff on all linking positions
	for _, rule in ipairs(mesecon.flattenrules(rules)) do
		local np = vector.add(pos, rule)
		local rulenames = mesecon.rules_link_rule_all(pos, rule)
		for _, rulename in ipairs(rulenames) do
			mesecon.vm_begin()
			mesecon.changesignal(np, minetest.get_node(np), rulename, mesecon.state.off, 2)

			-- Turnoff returns true if turnoff process was successful, no onstate receptor
			-- was found along the way. Commit changes that were made in voxelmanip. If turnoff
			-- returns true, an onstate receptor was found, abort voxelmanip transaction.
			if os then rulename.opaquespread=os end
			local res, rec_on = mesecon.turnoff(np, rulename)
			if (res) then
				mesecon.vm_commit()
			else
				mesecon.vm_abort()
				for _, rec in pairs(rec_on) do
					--rules??? -> mesecon.receptor_get_rules(node)
					local node = mesecon.get_node_force(rec)
					local meta = minetest.get_meta(rec)
					local voltage = meta:get_string("mesecon_voltage")~="" and meta:get_int("mesecon_voltage") or 15
					mesecon.receptor_on(rec, mesecon.receptor_get_rules(node), voltage)
				end
			end
		end
	end
end)

function mesecon.receptor_off(pos, rules, opaquespread)
	mesecon.queue:add_action(pos, "receptor_off", {rules, opaquespread}, nil, rules)
end

mesecon.queue:add_function("conductor_off", function (pos, rules, node)
	local n = node or mesecon.get_node_force(pos)
	local r = rules or mesecon.conductor_get_rules(n)
	for _, rule in ipairs(mesecon.flattenrules(r)) do
		mesecon.vm_begin()
		local np = vector.add(pos, rule)
		local res, rec_on = mesecon.turnoff(np, rule)
		if (res) then
			mesecon.vm_commit()
		else
			mesecon.vm_abort()
			for _, rec in pairs(rec_on) do
				local nn = mesecon.get_node_force(rec)
				local meta = minetest.get_meta(rec)
				local voltage = meta:get_string("mesecon_voltage")~="" and meta:get_int("mesecon_voltage") or 15
				mesecon.receptor_on(rec, mesecon.receptor_get_rules(nn), voltage)
			end
		end
	end
end)

function mesecon.conductor_off(pos, rules, node)
	mesecon.queue:add_action(pos, "conductor_off", {rules, node}, nil, rules)
end

--Services like turnoff receptor on dignode and so on
dofile(minetest.get_modpath("mesecons").."/services.lua");
